#pragma once
#include <string>
#include  <iostream>;

class person {
	/*protected:*/
	private:
		std::string name_ {""};
		int age_{0};
		char gender_{'\n'};

	public:
		person(std::string name, int age, char gender) {
			this->name_ = name;
			this->age_ = age;
			this->gender_ = gender;
		}

		friend std::ostream& operator<< (std::ostream& os, const person& p) {
			os << p.name_ << " " << p.age_ << " " << p.gender_;
			return os;
		}

		bool operator()() {
			if (gender_ == 'm' && age_ > 65) {
				return true;
			}

			else if (gender_ == 'f' && age_ > 65) {
					return true;
			}
			else {
				return false;
			}
		}


};