//import mysqrt;
#include  <iostream>;
#include <vector>
#include <ranges>
#include "person.h"
#include <string>
#include <algorithm>

// g++ person.cpp person.h -g -Wall -std=c++20

//using namespace std;
//
//int main(int argc, char** argv)
//{
//	//cout << "The result of sqrt(16) is "
//		//<< mysqrt::getMySqrt(16) << endl;
//	//cout << "The result of sqrt(2) is "
//		//<< mysqrt::getMySqrt(2) << endl;
//
//	vector vec{ 1,2,3,4,5,6 };
//
//	auto v = vec |
//		views::filter([](auto const i) {return i % 2 == 0; }) | views::transform([](auto const i) {return i * i; });
//	ranges::copy(v.begin(), v.end(), ostream_iterator<int>(cout, " "));
//	return 0;
//}

int main(int argc, char** argv) {

	std::vector v{ person((std::string)"Fred", 65, 'm'),
					person((std::string)"Betty", 66, 'f'),
					person((std::string)"Barney", 60, 'm'),
					person((std::string)"Wilma", 61, 'f'),
					person((std::string)"Bambam", 10, 'm')};
	std::ranges::copy_if(v, std::ostream_iterator<person>(std::cout, "\n"), [](auto p) {return p(); });
	
		
	return 0;
}


